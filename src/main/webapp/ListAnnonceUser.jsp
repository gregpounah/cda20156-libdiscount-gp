<%@page import="fr.afpa.beans.Utilisateur"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
   <%@ page import="fr.afpa.beans.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Espace User</title>
</head>
<body>
		
	<c:if test="${ !empty sessionScope.utilisateur}">
	
		<p>Bienvenue ${ sessionScope.utilisateur.prenom }</p>
	</c:if>
	
		<p> Liste Annonce :</p>
		<br><br>
		
		<c:forEach var="item" items="${sessionScope.listAnnonceUser}" varStatus="status">
		<tr>
		   <td> ${status.index} : ${item}</td>
		</tr>
		</c:forEach>
		  
		<br>
	<a href="espaceuser.jsp" style="text-align : center;">Retour à l'accueil</a><br>
	
</body>
</html>