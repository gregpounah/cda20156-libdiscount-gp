package fr.afpa.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString

@Entity
@NamedQuery(name="Verif_utilisateur",query="select u from Utilisateur u where u.login=:val")
public class Utilisateur {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_utilisateur")
	private int id_utilisateur;
	
	@Column(name="login", length= 10, unique= true)
	private String login;
	
	@Column(name="password",nullable = false, length= 20)
	private String password;
	
	@Column(name="nom",nullable = false, length= 30)
	private String nom;
	
	@Column(name="prenom",nullable = false, length= 30)
	private String prenom;
	
	@Column(name="mail",nullable = false, unique= true)
	private String mail;
	
	@Column(name="noPhone")
	private String noPhone;
	
	@Column(name="adresse")
	private String adresse;
	
	
	

	public Utilisateur(String login, String password, String nom, String prenom, String mail,
			String noPhone, String adresse) {
		super();
		
		this.login = login;
		this.password = password;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.noPhone = noPhone;
		this.adresse = adresse;
	
	}
	
	
	

	

}
