package fr.afpa.beans;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@NoArgsConstructor
@ToString

@Entity
@NamedQuery(name="recupAnnonce",query="select a from Annonce a where a.emetteur=:val")
public class Annonce {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "annonce_id_generator")
	@SequenceGenerator(name = "annonce_id_generator", sequenceName = "annonce_seq", allocationSize = 1, initialValue = 1)
	@Column(name = "id_annonce")
	private int idAnnoce;
	
	@Column(nullable = false, length= 30)
	private String titre;
	
	@Column(nullable = false, length= 30)
	private String isbn;
	
	@Column(nullable = false, length= 30)
	private String niveauScolaire;
	
	@Column(nullable = false)
	private String date;
	
	@Column(nullable = false, length= 30)
	private String maisonEdition;
	
	@Column(nullable = false)
	private int prix;
	
	@Column(nullable = false)
	private int quantite;
	
	@Column(nullable = false)
	private int remise;
	
	@Column(nullable = false)
	private int prixtotal;
	
	
	
	@Column
	private String emetteur;






	public Annonce(String titre, String isbn, String niveauScolaire, String date, String maisonEdition, int prix,
			int quantite, int remise, int prixtotal, String emetteur) {
		super();
		this.titre = titre;
		this.isbn = isbn;
		this.niveauScolaire = niveauScolaire;
		this.date = date;
		this.maisonEdition = maisonEdition;
		this.prix = prix;
		this.quantite = quantite;
		this.remise = remise;
		this.prixtotal = prixtotal;
		this.emetteur = emetteur;
	}
	
	
	
}
