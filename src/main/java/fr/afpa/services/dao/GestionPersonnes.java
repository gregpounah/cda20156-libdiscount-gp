package fr.afpa.services.dao;

import java.util.ArrayList;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Personne;
import fr.afpa.beans.Utilisateur;
import fr.afpa.session.HibernateUtils;

public class GestionPersonnes {

	private Session s;

	public void enregistrerPersonne(Personne per) {

		// Ouverture d'une session Hibernate
		s = HibernateUtils.getSession();

		// D�but de la transaction
		Transaction tx = s.beginTransaction();
		s.persist(per);

		tx.commit();

		s.close();
	}
	

	public void enregistrerPersonne(Utilisateur per) {

		// Ouverture d'une session Hibernate
		s = HibernateUtils.getSession();

		// D�but de la transaction
		Transaction tx = s.beginTransaction();
		s.persist(per);

		tx.commit();

		s.close();
	}

	public ArrayList<Personne> listPersonnes() {
		// Ouverture d'une session Hibernate
		s = HibernateUtils.getSession();
		// Cr�ation de la requ�te
		Query q = s.createQuery("from Personne");

		@SuppressWarnings("unchecked")
		ArrayList<Personne> listPersonnes = (ArrayList<Personne>) q.getResultList();
		return listPersonnes;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Annonce> listAnnonce(String nom) {
		// Ouverture d'une session Hibernate
		s = HibernateUtils.getSession();
		
		Query q = s.getNamedQuery("recupAnnonce");

		q.setParameter("val", nom);
		
		ArrayList<Annonce> listAnnonce = (ArrayList<Annonce>) q.getResultList();
		return listAnnonce;
	}
	
	
	public Utilisateur recupPersonneParNom(String nom) {
		// Ouverture d'une session Hibernate
			s = HibernateUtils.getSession();
				
				Query q = s.getNamedQuery("Verif_utilisateur");
				
			    q.setParameter("val", nom);
				
				Utilisateur personne = (Utilisateur) q.getSingleResult();
				
				s.close();
				return personne;
	}
	
	public void enregistrerAnnonce(Annonce per) {

		// Ouverture d'une session Hibernate
		s = HibernateUtils.getSession();

		// D�but de la transaction
		Transaction tx = s.beginTransaction();
		s.persist(per);

		tx.commit();

		s.close();
	}

}
