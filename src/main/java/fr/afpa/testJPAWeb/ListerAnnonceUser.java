package fr.afpa.testJPAWeb;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Personne;
import fr.afpa.services.dao.GestionPersonnes;

/**
 * Servlet implementation class ListerAnnonceUser
 */
public class ListerAnnonceUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListerAnnonceUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		   /* Récupération de la session depuis la requête */
        HttpSession session = request.getSession();
        
        String userannone =  (String) session.getAttribute("userannonce");
		
		ArrayList<Annonce> listAnnonceUser = new GestionPersonnes().listAnnonce(userannone);
		
		for (Annonce annonce: listAnnonceUser) {
			System.err.println(annonce);
		}

    	
   	 	session.setAttribute("listAnnonceUser", listAnnonceUser);
   	 	
   	 this.getServletContext().getRequestDispatcher("/ListAnnonceUser.jsp").forward(request, response);
	}

}
