package fr.afpa.testJPAWeb;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Personne;
import fr.afpa.beans.Utilisateur;

import fr.afpa.services.dao.GestionPersonnes;

/**
 * Servlet implementation class PosterAnnonce
 */
public class PosterAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String titre = request.getParameter("titre");
		String niveauscolaire = request.getParameter("niveauscolaire");
		String isbn = request.getParameter("isbn");
		String date  = request.getParameter("date");
		String maisonedition = request.getParameter("maisonedition");
		String prix =  request.getParameter("prix");
		String quantite = request.getParameter("quantité");
		String remise = request.getParameter("remise");
		String prixtotal = request.getParameter("prix total");
		
	
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String titre = request.getParameter("titre");
		String niveauscolaire = request.getParameter("niveauScolaire");
		String isbn = request.getParameter("isbn");
		String date  = request.getParameter("date");
		String maisonedition = request.getParameter("maisonedition");
		String prix =  request.getParameter("prix");
		String quantite = request.getParameter("quantité");
		String remise = request.getParameter("remise");
		String prixtotal = request.getParameter("prix total");
		
		
		/*
		 * on convertie les string en int 
		 * 
		 * int price = Integer.parseInt(prix);
		int quantity = Integer.parseInt(quantite);
		int refund = Integer.parseInt(remise);
		int pricetotal = Integer.parseInt(prixtotal)
		 * 
		 * */
		System.out.println(quantite);
		
		int price = 0;
		int quantity = 0;
		int refund = 0;
		int pricetotal = 0;
		
		
		   /* Récupération de la session depuis la requête */
        HttpSession session = request.getSession();
        
    	String userannone =  (String) session.getAttribute("userannonce");
    	
    	
    	
    	Annonce post = new Annonce(titre, isbn, niveauscolaire, date, maisonedition, price, quantity, refund, pricetotal, userannone);
    	
    	new GestionPersonnes().enregistrerAnnonce(post);
    	response.sendRedirect("espaceuser.jsp");
		
	}

}
