package fr.afpa.testJPAWeb;

import java.io.IOException;
import java.util.ArrayList;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

import fr.afpa.beans.Personne;
import fr.afpa.services.dao.GestionPersonnes;
import fr.afpa.session.HibernateUtils;

/**
 * Servlet implementation class ListerPersonnes
 */
public class ListerPersonnes extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
					
					ArrayList<Personne> listPersonnes = new GestionPersonnes().listPersonnes();
					// Affichage de la liste de résultats
					for (Personne personne: listPersonnes) {
						System.err.println(personne);
					}
	}


}
