package fr.afpa.testJPAWeb;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import fr.afpa.beans.Utilisateur;
import fr.afpa.services.dao.GestionPersonnes;




/**
 * Servlet implementation class CreationPersonne
 */
public class CreationPersonne extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nom = request.getParameter("nom");
		String prenom =  request.getParameter("prenom");
		
						
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
		String nom = request.getParameter("nom");
		String prenom =  request.getParameter("prenom");
		String login = request.getParameter("login");
		String mdp =  request.getParameter("password");
		String mail = request.getParameter("mail");
		String adresse =  request.getParameter("adresse");
		String tel = request.getParameter("noPhone");
		
		Utilisateur user = new Utilisateur(login,mdp,nom,prenom,mail,tel,adresse);
		
		
		
		
		new GestionPersonnes().enregistrerPersonne(user);
		response.sendRedirect("Confirmation.jsp");
		
	}

	
}
