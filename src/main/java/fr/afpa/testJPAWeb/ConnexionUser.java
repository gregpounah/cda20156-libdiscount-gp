package fr.afpa.testJPAWeb;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;

import fr.afpa.beans.Utilisateur;
import fr.afpa.services.dao.GestionPersonnes;


/**
 * Servlet implementation class ConnexionUser
 */
public class ConnexionUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
	
		String login = request.getParameter("login");
		String mdp =  request.getParameter("password");
		
		Utilisateur user = new GestionPersonnes().recupPersonneParNom(login);
		
		System.out.println(user);
	
		
	    /* Récupération de la session depuis la requête */
        HttpSession session = request.getSession();

        session.setAttribute("utilisateur", user);
        
        session.setAttribute("userannonce", user.getLogin());
			
		this.getServletContext().getRequestDispatcher("/espaceuser.jsp").forward(request, response);

	}

}
